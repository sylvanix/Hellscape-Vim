
" use colors as defined in ~/.vim/colors/
colorscheme hellscape

" font & font size
set guifont=Hack\ 13

" default window size
set lines=54
set columns=129

" right column color & position
set colorcolumn=99
"highlight ColorColumn ctermbg=0 guibg=grey20
set textwidth=99
set wrap

" Insert space characters whenever the tab key is pressed (not tabs).
" - to insert real tabs, like in a Makefile, use the Ctrl-V <Tab> sequence.
set tabstop=4 shiftwidth=4 expandtab

" misc
set number
set ignorecase
set shiftwidth=4
set softtabstop=4
set ts=4
"set spell spelllang=en_uk

" remap the esc to jj
"inoremap jj <ESC>

" display the status line
set laststatus=2

" format the tab label
" %N ---> tab number
" %t ---> file name
" %M ---> shows a + sign if modified
set guitablabel=\[%N\]\%t\ %M


set hlsearch
" Press Space to turn off highlighting and clear any message already displayed
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

syntax on

" no fucking error bell!
set belloff=all
